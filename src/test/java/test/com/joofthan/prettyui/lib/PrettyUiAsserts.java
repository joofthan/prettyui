package test.com.joofthan.prettyui.lib;

import org.opentest4j.AssertionFailedError;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PrettyUiAsserts {
    public static void assertContains(String expected, String result){
        try{
            assertTrue(result.contains(expected));
        }catch (AssertionFailedError e){
            throw new AssertionFailedError("Result should contain: \""+expected+"\"", expected, result);
        }
    }

    public static void assertNotContains(String expected, String result){
        try{
            assertFalse(result.contains(expected));
        }catch (AssertionFailedError e){
            throw new AssertionFailedError("Result should not contain: \""+expected+"\"", expected, result);
        }
    }
}
