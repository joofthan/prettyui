package test.com.joofthan.prettyui.lib;

import com.joofthan.prettyui.PrettyUiFactory;
import com.joofthan.prettyui.core.UiElement;

import static test.com.joofthan.prettyui.lib.PrettyUiAsserts.assertContains;
import static test.com.joofthan.prettyui.lib.PrettyUiAsserts.assertNotContains;

public class StyleTest<T extends UiElement> {

    protected T object;

    protected void shouldNotContain(String expected) {
        assertNotContains(expected, result(object));
    }

    protected void shouldContain(String expected) {
        assertContains(expected, result(object));
    }

    protected String result(T ob) {
        return ob.generateCSS(PrettyUiFactory.createTestConfig().renderContext());
    }

}
