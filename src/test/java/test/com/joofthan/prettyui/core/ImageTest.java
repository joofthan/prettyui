package test.com.joofthan.prettyui.core;

import com.joofthan.prettyui.core.elements.Image;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import test.com.joofthan.prettyui.lib.StyleTest;

import static com.joofthan.prettyui.core.style.Styles.BORDER_BLACK;
import static com.joofthan.prettyui.core.style.Styles.GLOW_BLUE;


class ImageTest extends StyleTest<Image> {
    @BeforeEach
    void setUp() {
        reset();
    }

    private void reset() {
        object = new Image("test.png")
                .withSize(100,100);
    }

    @Test
    void testInitialized(){
        shouldContain("#prettyui1 {\n" +
                "  position: absolute;\n" +
                "  left: 0vw;\n" +
                "  top: 0vw;\n" +
                "  width: 100vw;\n" +
                "  height: 100vw;\n" +
                "  transition: 250ms;\n" +
                "}\n" +
                "\n" +
                "#prettyui1:hover {\n" +
                "}\n" +
                "#prettyui1:selected {\n" +
                "}");
    }

    @Test
    @Disabled("Immulable not any more. Too complex.")
    void testImmutable() {
        object.withPosition(10,10);
        shouldNotContain("left: 10vw;");

        object = object.withPosition(10,10);
        shouldContain("left: 10vw;");
    }

    @Test
    void testWith() {
        object = object.withOnHoverStyle(GLOW_BLUE)
                       .withOnHoverStyle(BORDER_BLACK);
        shouldContain("border-color: #000000;");
    }

    /**

     @Test
     void test(){
     object;
     shouldContain("");
     }
     */
}