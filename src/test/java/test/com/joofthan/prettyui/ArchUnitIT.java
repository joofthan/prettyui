package test.com.joofthan.prettyui;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.lang.ArchRule;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;
import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

public class ArchUnitIT {

    private static JavaClasses classes;

    @BeforeAll
    static void init(){
        classes = new ClassFileImporter().importPackages("com.joofthan.prettyui");
    }

    @Test
    void classesShouldNotDependOnAdapter(){
        ArchRule myRule = noClasses()
                .that().resideOutsideOfPackages("..prettyui.demoapp..","..adapter..")
                .and().doNotHaveSimpleName("PrettyUiFactory")
                .should().dependOnClassesThat()
                .resideInAnyPackage("..prettyui.demoapp..", "..adapter..");

        myRule.check(classes);
    }

    @Test
    void coreClassesShouldNotDependOnOutside(){
        ArchRule myRule = classes()
                .that().resideInAnyPackage("..core.." )
                .should().onlyAccessClassesThat()
                .resideInAnyPackage("..core..", "java.lang..","java.util..");

        myRule.check(classes);
    }

    @Test
    void testOnlyAdapterToOutside(){
        ArchRule myRule = classes()
                .that().resideOutsideOfPackage("..adapter..")
                .should().onlyAccessClassesThat()
                .resideInAnyPackage("..prettyui..","java.lang..","java.util..");

        myRule.check(classes);
    }

}
