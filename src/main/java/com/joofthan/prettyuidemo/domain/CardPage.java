package com.joofthan.prettyuidemo.domain;

import com.joofthan.prettyui.PrettyUi;
import com.joofthan.prettyui.core.PageBuilder;
import com.joofthan.prettyui.core.UiElement;
import com.joofthan.prettyui.core.UiPage;
import com.joofthan.prettyui.core.elements.Image;
import com.joofthan.prettyui.core.elements.Text;
import com.joofthan.prettyui.core.style.Style;
import com.joofthan.prettyui.core.types.Unit;

import static com.joofthan.prettyui.core.style.Styles.GLOW_BLUE;
import static com.joofthan.prettyui.core.types.Color.RED;

public class CardPage {
    private final PrettyUi prettyUi;
    private Text text;

    public CardPage(PrettyUi prettyUi) {
        this.prettyUi = prettyUi;
    }

    public UiPage render(){
        PageBuilder page = prettyUi.page(prettyUi.renderContext().withUnit(Unit.VIEW_HEIGTH)).withTitle("Card Demo");

        Style hoverStyle = new Style(GLOW_BLUE)
                                    .withScale(1.5)
                                    .withRotation(10)
                                    .withPositionOffset(0, -2);

        Image first = new Image("http://joofthan.com/images/engine.png")
                            .withSize(10, 15)
                            .withPosition(10, 80);
        Image previous = first;
        page.withElement(first);
        for (int i = 0; i < 20; i++) {
            previous = new Image("http://joofthan.com/images/engine.png")
                            .withSize(10, 15)
                            .withPosition(previous.getRightX()+1, 80)
                            .onClickListener(self -> {
                                if(self.getTopY() > 15)self.setPosition(self.getLeftX(), self.getTopY()-15);
                                self.addStyle(hoverStyle.withRotation(0).withScale(1).withPositionOffset(0,0));
                                self.addHoverStyle(hoverStyle.withRotation(0).withScale(1).withShadowColor(RED).andShadowBlur(2));
                                first.setPosition(first.getLeftX(), self.getTopY()+15);
                                first.addStyle(new Style().withRotation(90).withPositionOffset(-10,5));
                                showPosition(self);
                            })
                            .onUnHoverListener(self-> {
                                if(self.getBottomY() < 85){
                                    self.setPosition(self.getLeftX(), self.getTopY()+15);
                                }
                                showPosition(self);
                            })
                            .withAnimationDuration(150)
                            .withOnHoverStyle(hoverStyle);

            page.withElement(previous);
        }

        text = new Text("Position: ")
                    .withPosition(0,0)
                    .withFontSize(3)
                    .withSize(100,5);
        page.withElement(text);

        UiPage uiPage = page.build();

        prettyUi.addPage(uiPage);
        return uiPage;
    }


    private void showPosition(UiElement e){
        text.setValue("Position: x:"+e.getLeftX()+" y:" +e.getTopY());
    }
}
