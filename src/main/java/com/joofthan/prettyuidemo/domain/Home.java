package com.joofthan.prettyuidemo.domain;

public class Home {

    public String generateHTML() {
        return "<a href=\"demo-page\">Demo Page</a><br>" +
                "<a href=\"card-page\">Card Page</a><br>" +
                "<a href=\"card-page-shared\">Shared State</a><br>";
    }
}
