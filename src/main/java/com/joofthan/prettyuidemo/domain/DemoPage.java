package com.joofthan.prettyuidemo.domain;

import com.joofthan.prettyui.PrettyUi;
import com.joofthan.prettyui.core.UiPage;
import com.joofthan.prettyui.core.elements.Box;
import com.joofthan.prettyui.core.elements.Checkbox;
import com.joofthan.prettyui.core.elements.Image;
import com.joofthan.prettyui.core.elements.Text;
import com.joofthan.prettyui.core.style.CustomStyle;
import com.joofthan.prettyui.core.style.Style;
import com.joofthan.prettyui.core.types.Color;

import static com.joofthan.prettyui.core.style.BorderStyle.DOTTED;
import static com.joofthan.prettyui.core.style.BorderStyle.SOLID;
import static com.joofthan.prettyui.core.style.Styles.*;
import static com.joofthan.prettyui.core.types.Color.*;

public class DemoPage {

    private final PrettyUi prettyUi;

    public DemoPage(PrettyUi prettyUi) {
        this.prettyUi = prettyUi;
    }

    public UiPage render(){
        Style demoTemplate = new Style()
                                .withBorderType(SOLID)
                                    .andBorderRadius(5)
                                    .andBorderWidth(1)
                                    .andBorderColor(Color.of("#33F666"))
                                .withShadowColor(GREEN)
                                    .andShadowMovedToRight(3)
                                    .andShadowMovedToBottom(2)
                                    .andShadowSize(1)
                                    .andShadowBlur(5);

        Image image1 = new Image("http://joofthan.com/images/engine.png")
                                .withSize(50, 25)
                                .withPosition(0, 0)
                                .withStyle(demoTemplate)
                                .withAnimationDuration(150)
                                .withOnHoverStyle(
                                        demoTemplate.withBorderType(SOLID).andBorderRadius(10)
                                )
                                .withOnHoverStyle(GLOW_BLUE
                                                    .and(MOVE_LEFT)
                                                    .and(BORDER_BLACK)
                                )
                                .withOnSelectStyle(GLOW_GREEN
                                                    .and(MOVE_UP)
                                )
                                .onSelectListener((self) -> self.setImage("test2.png"))
                                .onUnSelectListener((self) -> self.setImage("test.png"))
                                .onClickListener((self) -> self.setPosition(self.getLeftX()+2, 10));

        Image image2 = new Image("http://joofthan.com/images/engine.png")
                            .withSize(50, 25)
                            .withPosition(image1.getRightX(), image1.getBottomY())
                            .withStyle(demoTemplate.withBorderType(SOLID).andBorderColor(RED))
                            .withOnHoverStyle(demoTemplate.withBorderType(DOTTED).andBorderColor(RED))
                            .onClickListener(e-> prettyUi.getOutputText().text("Image2 Clicked Java"))
                            .onClickListener(e -> System.out.println("Image: "+e))
                            .onHoverListener(e-> e.setPosition(e.getLeftX(), e.getTopY()+10))
                            ;

        Box box = new Box()
                .withColor(RED)
                .withStyle(new CustomStyle("b"))
                .withSize(10, 10)
                .withPosition(10, 0);

        Text text = new Text("hallo")
                            .withSize(box.getWidth(), box.getHeight())
                            .withPosition(box.getLeftX(), box.getTopY())
                            .withColor(GREEN)
                            .withFontSize(5);

        Checkbox checkbox = new Checkbox()
                .withPosition(80, 4)
                .withSize(5, 5)
                .onClickListener(e -> {
                    if(e.getSelected()){
                        text.setColor(BLACK);
                    }else{
                        text.setColor(GREEN);
                    }
                });

        UiPage uiPage = prettyUi.page()
                .withTitle("Hello")
                .withElement(image1)
                .withElement(image2)
                .withElement(box)
                .withElement(text)
                .withElement(checkbox)
                .build();

        prettyUi.addPage(uiPage);

        return uiPage;
    }

}
