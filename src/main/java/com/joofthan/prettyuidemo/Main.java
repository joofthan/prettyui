package com.joofthan.prettyuidemo;

import com.joofthan.prettyui.PrettyUiFactory;
import com.joofthan.prettyui.adapter.server.EmbeddedWebServer;
import com.joofthan.prettyuidemo.application.PageHandler;

import java.io.IOException;


public class Main {
    private static final String GATEWAY_URL = "/gatewayapi";
    private static final String CONTEXT_PATH = "/";
    private static final int PORT = 8080;


    public static void main(String[] args) {
        var config = PrettyUiFactory.createDefaultConfig(GATEWAY_URL);
        var logger = config.getOutputText();
        var server = new EmbeddedWebServer(CONTEXT_PATH, PORT, logger);
        var pageHandler = new PageHandler(config);
        server.setRequestListener(http -> pageHandler.pageSwitch(http));
        try {
            server.start();
        } catch (IOException e) {
            System.out.println("App could not be started.");
            e.printStackTrace();
        }
    }
}
