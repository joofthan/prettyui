package com.joofthan.prettyuidemo.application;

import com.joofthan.prettyui.PrettyUi;
import com.joofthan.prettyui.core.UiPage;
import com.joofthan.prettyui.core.types.response.Content;
import com.joofthan.prettyui.core.types.response.HTML;
import com.joofthan.prettyuidemo.domain.CardPage;
import com.joofthan.prettyuidemo.domain.Home;
import com.joofthan.prettyuidemo.domain.DemoPage;
import com.sun.net.httpserver.HttpExchange;

import java.nio.charset.StandardCharsets;

public class PageHandler {
    private final PrettyUi prettyUi;
    private final String gatewayUrl;
    private final UiPage shared;

    public PageHandler(PrettyUi prettyUi) {
        this.prettyUi = prettyUi;
        this.gatewayUrl = prettyUi.gatewayUrl();
        shared = new CardPage(prettyUi).render();
        prettyUi.addPage(shared);
    }

    public Content pageSwitch(HttpExchange http) {
        String url = http.getRequestURI().getPath();

        if(url.startsWith(gatewayUrl)){
            Content c = prettyUi.handleGatewayRequest(url);
            http.getResponseHeaders().set("Content-Type", c.type);
            return c;
        }
        http.getResponseHeaders().set("Content-Type" ,"text/html;charset=UTF-8");

        if("/demo-page".equals(url)){
            return new HTML(new DemoPage(prettyUi).render().generateHTML());
        }
        if("/card-page".equals(url)){
            return new HTML(new CardPage(prettyUi).render().generateHTML());
        }
        if("/card-page-shared".equals(url)){
            return new HTML(this.shared.generateHTML());
        }
        if("/".equals(url)){
            return new HTML(new Home().generateHTML());
        }

        throw new PageNotFoundException(http);
    }

}
