package com.joofthan.prettyuidemo.application;

import com.joofthan.prettyui.core.exception.PrettyUiException;
import com.sun.net.httpserver.HttpExchange;

class PageNotFoundException extends PrettyUiException {
    public PageNotFoundException(HttpExchange request) {
        super("404 Page \""+request.getRequestURI()+"\" not found.", null, "HttpExchange{ resource="+request.getRequestURI()+", method="+request.getRequestMethod()+", protocol="+ request.getProtocol() +"}");
    }
}
