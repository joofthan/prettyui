package com.joofthan.prettyui;

import com.joofthan.prettyui.adapter.io.OutputTextImpl;
import com.joofthan.prettyui.adapter.io.ResourceListImpl;
import com.joofthan.prettyui.adapter.io.ResourceProviderDummy;
import com.joofthan.prettyui.adapter.io.ResourceProviderImpl;
import com.joofthan.prettyui.core.RenderContext;
import com.joofthan.prettyui.core.io.Gateway;
import com.joofthan.prettyui.core.io.OutputText;

public class PrettyUiFactory {
    public static PrettyUi createDefaultConfig(String gatewayURL) {
        OutputText outputText = new OutputTextImpl();
        Gateway gateway = new Gateway(gatewayURL, outputText);
        return new PrettyUi(new ResourceProviderImpl(outputText), outputText, createRenderContext(gateway), gateway);
    }

    public static PrettyUi createTestConfig() {
        OutputText outputText = new OutputTextImpl();
        Gateway gateway = new Gateway("/dummyGateway", outputText);
        return new PrettyUi(new ResourceProviderDummy(), outputText, createRenderContext(gateway), gateway);
    }

    private static RenderContext createRenderContext(Gateway gateway) {
        return new RenderContext( new ResourceListImpl(gateway));
    }
}
