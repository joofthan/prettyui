package com.joofthan.prettyui.adapter;

import java.io.*;

public class SocketServer {
    public static void main(String[] args) {
        try {
            new SocketServer().start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void start() throws IOException {
        int port = 12345;
        java.net.ServerSocket serverSocket = new java.net.ServerSocket(port);
        java.net.Socket client = serverSocket.accept();


        String nachricht = leseNachricht(client);
        System.out.println(nachricht);
        schreibeNachricht(client, nachricht);
    }


    String leseNachricht(java.net.Socket socket) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(
                                                new InputStreamReader(
                                                    socket.getInputStream()
                                                )
                                        );
        char[] buffer = new char[200];
        int anzahlZeichen = bufferedReader.read(buffer, 0, 200); // blockiert bis Nachricht empfangen
        String nachricht = new String(buffer, 0, anzahlZeichen);
        return nachricht;
    }

    void schreibeNachricht(java.net.Socket socket, String nachricht) throws IOException {
        PrintWriter printWriter =
                new PrintWriter(
                        new OutputStreamWriter(
                                socket.getOutputStream()));
        printWriter.print(nachricht);
        printWriter.flush();
    }
}
