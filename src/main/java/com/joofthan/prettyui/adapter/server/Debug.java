package com.joofthan.prettyui.adapter.server;

import com.joofthan.prettyui.adapter.io.OutputTextImpl;
import com.joofthan.prettyui.core.types.response.Content;
import com.joofthan.prettyui.core.types.response.HTML;
import com.joofthan.prettyui.core.util.Util;

import java.io.IOException;

public class Debug {
    public void showInBrowser(Object... objects) {
        EmbeddedWebServer server = new EmbeddedWebServer("/", 8083, new OutputTextImpl());
        server.setRequestListener(e -> new HTML(Util.htmlFormattingObjects(objects)));
        try {
            server.start();
            Thread.sleep(10 * 1000);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            throw new EmbeddedWebServerException(e);
        }
    }
}
