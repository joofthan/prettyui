package com.joofthan.prettyui.adapter.server;

import com.joofthan.prettyui.core.io.OutputText;
import com.joofthan.prettyui.core.types.response.Content;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class EmbeddedWebServer {
    private final boolean showResponse = false;

    private final String contextPath;
    private final int port;
    private Function<HttpExchange, Content> requestListener;
    private final OutputText outputText;

    public EmbeddedWebServer(String contextPath, int port, OutputText outputText) {
        this.contextPath = contextPath;
        this.port = port;
        this.outputText = outputText;
    }

    public void setRequestListener(Function<HttpExchange, Content> requestListener) {
        this.requestListener = requestListener;
    }

    public void start() throws IOException {
        startServer();
    }

    private void startServer() throws IOException {
        outputText.info("Create Server");
        HttpServer server = HttpServer.create(new InetSocketAddress(port), 0);
        output("Create Handler");
        HttpRequestHandler handler = new HttpRequestHandler(requestListener, showResponse, outputText);
        output("Create Context");
        server.createContext(contextPath, handler);
        output("Create Default Executor");
        server.setExecutor(null); // creates a default executor
        output("Start Server");
        server.start();
        output("Start Server finished");
        outputText.info("Running on: http://localhost:" + port + contextPath);
    }

    private void output(String text){
        outputText.fine(text);
    }
}
