package com.joofthan.prettyui.adapter.server;

import com.joofthan.prettyui.core.exception.PrettyUiException;
import com.joofthan.prettyui.core.io.OutputText;
import com.joofthan.prettyui.core.types.response.Content;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.function.Function;


class HttpRequestHandler implements HttpHandler {
    private static final int HTTP_OK = 200;
    private static final int HTTP_NOT_FOUND = 404;
    private final Function<HttpExchange, Content> listener;
    private final boolean showResponse;
    private OutputText outputText;

    public HttpRequestHandler(Function<HttpExchange, Content> listener, boolean showResponse, OutputText outputText) {
        this.listener = listener;
        this.showResponse = showResponse;
        this.outputText = outputText;
    }

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        try {
            doHandle(httpExchange);
        } catch (Exception e) {
            e.printStackTrace();//catch all errors and print because HttpServer does hide some errors
            outputText.error(e.getMessage());
            if(e.getCause() != null)outputText.error("Caused by: "+e.getCause().getMessage());
            EmbeddedWebServerException myEx = new EmbeddedWebServerException(e);
            sendResponse(httpExchange, HTTP_NOT_FOUND, myEx.asHTML().getBytes(StandardCharsets.UTF_8));
            throw myEx;
        }
    }

    private void doHandle(HttpExchange httpExchange) throws IOException {
        int statusCode = HTTP_OK;
        fine("");
        fine("Received HttpRequest: \"" + httpExchange.getRequestURI().toString() + "\"");
        finest("Generate Page");
        byte[] content;
        try{
            content = generateContent(httpExchange);
        }catch (PrettyUiException e) {
            e.printStackTrace();
            outputText.error(e.getMessage());
            if(e.getCause() != null)outputText.error("Caused by: "+e.getCause().getMessage());
            content = e.asHTML().getBytes(StandardCharsets.UTF_8);
            statusCode = HTTP_NOT_FOUND;
        }
        finest("Generate Page finished");

        finest("Send Response" + (showResponse ? ": \n" : ""));
        sendResponse(httpExchange, statusCode, content);
        fine((showResponse ? content + "\n\n" : "") + "Send Response finished");
    }

    private byte[] generateContent(HttpExchange httpExchange){
        return listener.apply(httpExchange).content;
    }

    private void sendResponse(HttpExchange httpExchange, int code, byte[] content) throws IOException {
        httpExchange.sendResponseHeaders(code, content.length);
        OutputStream os = httpExchange.getResponseBody();
        os.write(content);
        os.close();
    }

    private void finest(String text) {
        outputText.finest(text);
    }

    private void fine(String text) {
        outputText.fine(text);
    }
}
