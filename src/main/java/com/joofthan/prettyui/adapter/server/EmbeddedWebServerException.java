package com.joofthan.prettyui.adapter.server;

import com.joofthan.prettyui.core.exception.PrettyUiException;

public class EmbeddedWebServerException extends PrettyUiException {
    public EmbeddedWebServerException(Exception e) {
        super("Page could not be rendered.", e);
    }
}
