package com.joofthan.prettyui.adapter.io;

import com.joofthan.prettyui.core.io.Gateway;
import com.joofthan.prettyui.core.io.Images;
import com.joofthan.prettyui.core.io.ResourceList;

import java.util.List;

public class ResourceListImpl implements ResourceList {
    private static final String SCRIPT = "scripts/";
    private static final String EVENT = "event/";

    private final Gateway gateway;
    public ResourceListImpl(Gateway gateway) {
        this.gateway = gateway;
    }
    @Override
    public List<String> logContent() {
        return gateway.outputText.log();
    }


    @Override
    public String libJs() {
        return gateway.url() + SCRIPT +"lib.mjs";
    }

    @Override
    public String eventEndpointURL() {
        return gateway.url()+EVENT;
    }

    @Override
    public Images img() {
        return new Images() {
            public static final String IMG = "img/";
            @Override
            public String check() {
                return gateway.url() + IMG +"check.png";
            }

            @Override
            public String checkHover() {
                return gateway.url() + IMG +"check_hover.png";
            }

            @Override
            public String checkEmpty() {
                return gateway.url() + IMG +"check_empty.png";
            }

            @Override
            public String checkEmptyHover() {
                return gateway.url() + IMG +"check_empty_hover.png";
            }

            @Override
            public String folderURL() {
                return gateway.url()+IMG;
            }
        };
    }
}
