package com.joofthan.prettyui.adapter.io;

import com.joofthan.prettyui.core.exception.PrettyUiException;
import com.joofthan.prettyui.core.io.OutputText;
import com.joofthan.prettyui.core.io.ResourceProvider;

import java.io.*;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

public class ResourceProviderImpl implements ResourceProvider {
    private final OutputText outputText;

    public ResourceProviderImpl(OutputText outputText) {
        this.outputText = outputText;
    }

    @Override
        public String getContent(String path) {
            try {
                String fileContent = new BufferedReader(new InputStreamReader(getFileContent(path)))
                        .lines()
                        .collect(Collectors.joining("\n"));

                return fileContent;
            } catch (IOException | URISyntaxException e) {
                throw new PrettyUiException()
                        .withMessage("\""+path+"\" not found.")
                        .withCause(e);
            }
        }

    @Override
    public byte[] getBytes(String path) {
        try {
            return getFileContent(path).readAllBytes();
        } catch (URISyntaxException | IOException e) {
            throw new PrettyUiException().withCause(e);
        }
    }

    private InputStream getFileContent(String path) throws URISyntaxException, IOException {
        InputStream is = getClass().getClassLoader().getResourceAsStream(path);
        if(is == null) throw new PrettyUiException()
                                    .withMessage("File \""+ path +"\" not found.")
                                    .withData(getClass().getName());

        return is;


    }
}
