package com.joofthan.prettyui.adapter.io;

import com.joofthan.prettyui.core.io.OutputText;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class OutputTextImpl implements OutputText {
    private boolean showFinest = false;
    private boolean showFine = true;
    private boolean showInfo = true;

    private List<String> log = new ArrayList<>();

    public void finest(String text){
        if(showFinest) {
            String message = "PrettyUi: "+time()+" FINEST: "+text;
            System.out.println(message);
            add(message);
        }
    }

    public void fine(String text){
        if(showFine) {
            String message = "PrettyUi: "+time()+" FINE: "+text;
            System.out.println(message);
            add(message);
        }
    }

    public void info(String text){
        if(showInfo){
            String message = "PrettyUi: "+time()+" INFO: "+text;
            System.out.println(message);
            add(message);
        }
    }
    public void warn(String text){
        String message = "PrettyUi: "+time()+" WARN: "+text;
        System.err.println(message);
        add(message);
    }
    public void error(String text){
        String message = "PrettyUi: "+time()+" ERROR: "+text;
        Logger.getLogger(OutputText.class.getName()).warning(message);
        System.err.println(message);
        add(message);
    }

    private String time(){
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss"));
    }

    @Override
    public void text(String text) {
        System.out.println(text);
        add(text);
    }

    @Override
    public List<String> log() {
        return log;
    }

    private void add(String text) {
        log.add(text);
    }
}
