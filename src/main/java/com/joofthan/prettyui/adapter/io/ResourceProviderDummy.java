package com.joofthan.prettyui.adapter.io;

import com.joofthan.prettyui.core.io.ResourceProvider;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class ResourceProviderDummy implements ResourceProvider {
    private final Map<String,String> resources = new HashMap<>();

    public ResourceProviderDummy(){
        resources.put("dummy/path", "DummyText");
    }

    @Override
    public String getContent(String path) {
        return resources.get(path);
    }

    @Override
    public byte[] getBytes(String path) {
        return getContent(path).getBytes(StandardCharsets.UTF_8);
    }
}
