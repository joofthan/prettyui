package com.joofthan.prettyui;

import com.joofthan.prettyui.core.PageBuilder;
import com.joofthan.prettyui.core.PageData;
import com.joofthan.prettyui.core.RenderContext;
import com.joofthan.prettyui.core.UiPage;
import com.joofthan.prettyui.core.elements.Image;
import com.joofthan.prettyui.core.exception.EventException;
import com.joofthan.prettyui.core.io.Gateway;
import com.joofthan.prettyui.core.io.OutputText;
import com.joofthan.prettyui.core.io.ResourceProvider;
import com.joofthan.prettyui.core.style.Style;
import com.joofthan.prettyui.core.types.Color;
import com.joofthan.prettyui.core.types.EventType;
import com.joofthan.prettyui.core.types.response.Content;
import com.joofthan.prettyui.core.types.response.JSON;
import com.joofthan.prettyui.core.types.response.JavaScript;
import com.joofthan.prettyui.core.types.response.PNG;

import java.util.HashMap;
import java.util.Map;

public class PrettyUi {
    private final ResourceProvider resourceProvider;
    private final OutputText outputText;
    private final RenderContext renderContext;
    private final Gateway gateway;
    private final Map<String, PageWrapper> pages = new HashMap<>();
    private int pageCount = 0;


    public PrettyUi(ResourceProvider resourceProvider, OutputText outputText, RenderContext renderContext, Gateway gateway) {
        this.resourceProvider = resourceProvider;
        this.outputText = outputText;
        this.renderContext = renderContext;
        this.gateway = gateway;
        instance = this;
    }

    public Image image(String path){
        return new Image(path);
    }

    public PageBuilder page() {
        return page(this.renderContext);
    }

    public PageBuilder page(RenderContext c) {
        pageCount++;
        return new PageBuilder(c, "page"+ pageCount);
    }

    public Style style() {
        return new Style();
    }

    public Color color(String hex){
        return Color.of(hex);
    }

    public OutputText getOutputText() {
        return outputText;
    }

    public RenderContext renderContext() {
        return renderContext;
    }

    public Content handleGatewayRequest(String url) {
        if(url.startsWith(renderContext.resources().eventEndpointURL())){
            return handleEvent(url.substring(renderContext.resources().eventEndpointURL().length()));
        }else if(url.startsWith(renderContext.resources().img().folderURL())) {
            String path = url.substring(gateway.url().length());
            byte[] content = resourceProvider.getBytes(path);
            return new PNG(content);
        }else {
            String path = url.substring(gateway.url().length());
            String content = resourceProvider.getContent(path);
            return new JavaScript(content);
        }
    }

    private Content handleEvent(String subUrl) {
        if(subUrl.split("/").length < 3) throw new EventException("Invalid event url: \""+subUrl+"\".");
        String pageId = subUrl.split("/")[0];
        String elementId = subUrl.split("/")[1];
        String requestEvent = subUrl.split("/")[2];
        outputText.fine("Event \""+requestEvent+"\" recieved: Element with id \""+elementId+"\". On Page \""+pageId+"\".");

        if(!pages.containsKey(pageId)){
            throw new EventException("pages.get("+ pageId+") does not contain id: "+pageId+". Restart of Server does empty this map.", pages);
        }
        PageWrapper entry = pages.get(pageId);
        entry.page.handleEvent(elementId, EventType.parse(requestEvent));
        PageData data = entry.page.generatePageData();
        JSON eventResponse = new JSON()
                                .with("success", true)
                                .with("pageChanged", !data.equals(entry.cache))
                                .with("html", data.html)
                                .with("css", data.css)
                                .with("log", this.renderContext.resources().logContent());
        entry.cache = data;

        return eventResponse;
    }

    public void addPage(UiPage page) {
        this.pages.put(page.getId(), new PageWrapper(page));
    }

    public String gatewayUrl() {
        return gateway.url();
    }

    private static class PageWrapper {
        UiPage page;
        PageData cache;

        public PageWrapper(UiPage page) {
            this.page = page;
        }
    }

    public static PrettyUi instance;

    @Deprecated
    public static PrettyUi getInstance(){
        return instance;
    }
}
