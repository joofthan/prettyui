package com.joofthan.prettyui.core;

import com.joofthan.prettyui.core.io.ResourceList;
import com.joofthan.prettyui.core.types.Unit;


public class RenderContext {

    private final int id;
    private final Unit unit;
    private final String leftIndent;
    private final ResourceList resourceList;

    public RenderContext(ResourceList resourceList){
        this(1, Unit.VIEW_WIDTH, "",  resourceList);
    }

    private RenderContext(int id, Unit unit, String leftIndent, ResourceList resourceList) {
        this.resourceList = resourceList;

        this.id = id;
        this.unit = unit;
        this.leftIndent = leftIndent;
    }
    
    public String elementId(){
        return "prettyui"+this.id;
    }
    
    public RenderContext nextIdContext(){
        return new RenderContext(this.id+1, unit, leftIndent, resourceList);
    }

    public Unit getUnit() {
        return unit;
    }

    public String indent() {
        return this.leftIndent;
    }

    public String line(String line) {
        return indent() + line + "\n";
    }

    public RenderContext withLeftIndent(String leftIndent) {
        return new RenderContext(this.id, this.unit, leftIndent, resourceList);
    }

    public RenderContext addLeftIndent(String add) {
        return withLeftIndent(leftIndent+add);
    }

    public RenderContext withUnit(Unit unit) {
        return new RenderContext(this.id, unit, leftIndent, resourceList);
    }

    public ResourceList resources() {
        return resourceList;
    }
}
