package com.joofthan.prettyui.core.types;

import com.joofthan.prettyui.core.exception.PrettyUiException;

class InvalidValueException extends PrettyUiException {
    public InvalidValueException(String message) {
        super(message);
    }
    public InvalidValueException(String message, Object... object) {
        super(message, null, object);
    }
}
