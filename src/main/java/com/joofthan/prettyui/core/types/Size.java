package com.joofthan.prettyui.core.types;

public class Size implements DecimalNumber{
    public static final Size ZERO = Size.of(0);
    public static final Size NONE = Size.of(-1);
    private final double value;

    private Size(double value) {
        this.value = value;
    }

    public static Size of(double value) {
        return new Size(value);
    }

    public Size add(Size other) {
        return new Size(this.getDoubleValue() + other.getDoubleValue());
    }

    public Size multiply(double multiplier){
        return new Size(this.getDoubleValue() * multiplier);
    }

    public Size subtract(Size other) {
        return Size.of(this.getDoubleValue() - other.getDoubleValue());
    }

    public String getValue() {
        if(value % 1 == 0) {
            return getIntegerValue()+"";
        }else{
            return getDoubleValue()+"";
        }
    }

    public double getDoubleValue() {
        return value;
    }

    public int getIntegerValue(){
        return (int) value;
    }


    @Override
    public String toString() {
        return "Size{" +
                "value=" + value +
                '}';
    }
}
