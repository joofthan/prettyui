package com.joofthan.prettyui.core.types.response;

import com.joofthan.prettyui.core.types.response.Content;

public class PNG extends Content {
    public PNG(byte[] content) {
        super("image/png", content);
    }
}
