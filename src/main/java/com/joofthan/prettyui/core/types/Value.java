package com.joofthan.prettyui.core.types;

public interface Value {
    String getValue();
}
