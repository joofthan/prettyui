package com.joofthan.prettyui.core.types;

public class Scale extends AbstractValueType<Double> implements DecimalNumber {

    public Scale(double value) {
        super(value);
    }

    public static Scale of(double value) {
        return new Scale(value);
    }

    @Override
    public double getDoubleValue() {
        return value;
    }

    @Override
    public int getIntegerValue() {
        return value.intValue();
    }
}
