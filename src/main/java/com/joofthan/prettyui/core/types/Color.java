package com.joofthan.prettyui.core.types;

import java.util.regex.Pattern;

public class Color {
    public static Color BLACK = Color.of("#000000");
    public static Color RED = Color.of("#FF0000");
    public static Color GREEN = Color.of("#00FF00");
    public static Color BLUE = Color.of("#0000FF");
    public static Color YELLOW = Color.of("#00FFFF");
    public static Color WHITE = Color.of("#FFFFFF");

    private final String value;

    Color(String value) {
        this.value = value;
    }

    public static Color of(String hexValue) {
        if (hexValue == null || !Pattern.matches("#[\\dA-F]{6}", hexValue)) {
            throw new InvalidHexCodeException(hexValue);
        }
        return new Color(hexValue);
    }

    public String getHexCode() {
        return this.value;
    }

    @Override
    public String toString() {
        return "Color{" +
                "value='" + value + '\'' +
                '}';
    }
}
