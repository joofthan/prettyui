package com.joofthan.prettyui.core.types;

public abstract class AbstractValueType<T> implements Value{
    protected final T value;
    public AbstractValueType(T value) {
        if(value == null){
            throw new InvalidValueException("Value can not be empty.", this);
        }
        this.value = value;
    }
    public String getValue() {
        return value.toString();
    }

    @Override
    public String toString() {
        return "AbstractValueType{" +
                "value=" + value +
                '}';
    }
}
