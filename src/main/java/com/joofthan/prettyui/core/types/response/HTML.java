package com.joofthan.prettyui.core.types.response;

import java.nio.charset.StandardCharsets;

public class HTML extends Content {
    public HTML(String content) {
        super("text/html;charset=UTF-8", content.getBytes(StandardCharsets.UTF_8));
    }
}
