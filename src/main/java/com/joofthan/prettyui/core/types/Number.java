package com.joofthan.prettyui.core.types;

public interface Number extends Value{
    String getValue();
    int getIntegerValue();
}
