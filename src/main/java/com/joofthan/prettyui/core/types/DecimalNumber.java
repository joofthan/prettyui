package com.joofthan.prettyui.core.types;

public interface DecimalNumber extends Number {
    String getValue();
    double getDoubleValue();
}
