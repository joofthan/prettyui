package com.joofthan.prettyui.core.types;

public class Rotation extends AbstractValueType<Integer> implements Number {
    public Rotation(int value) {
        super(value);
    }

    public static Rotation of(int value) {
        if(value < 0 || value > 360){
            throw new InvalidValueException("Rotation is not between 0 and 360", value);
        }
        return new Rotation(value);
    }

    @Override
    public int getIntegerValue() {
        return value;
    }
}
