package com.joofthan.prettyui.core.types;

import com.joofthan.prettyui.core.exception.PrettyUiException;

class InvalidHexCodeException extends PrettyUiException {
    public InvalidHexCodeException(String hexcode) {
        super("Invalid hex code \"" + hexcode + "\".");
    }
}
