package com.joofthan.prettyui.core.types;

import com.joofthan.prettyui.core.exception.EventException;

import java.util.Arrays;

/**
 * JS equivalent in resources/scripts/EventType.mjs
 */
public enum EventType {
    ON_CLICK, ON_HOVER, ON_SELECT, ON_UNHOVER, ON_UNSELECT;

    public static EventType parse(String requestEvent) {
        EventType eventType = null;
        for (EventType e: EventType.values()) {
            if(e.jsName().equals(requestEvent)){
                eventType = e;
            }
        }
        if(eventType == null){
            throw new EventException("Event \""+requestEvent+"\" not found in EventType", Arrays.stream(EventType.values()).toArray());
        }

        return eventType;
    }

    public String jsName() {
        return name().replaceAll("ON_", "").toLowerCase() + "ed";
    }

    @Override
    public String toString() {
        return name() +": "+ jsName();
    }
}
