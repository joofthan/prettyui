package com.joofthan.prettyui.core.types.response;

import java.nio.charset.StandardCharsets;

public class JavaScript extends Content {
    public JavaScript(String content) {
        super("text/javascript;charset=UTF-8", content.getBytes(StandardCharsets.UTF_8));
    }
}
