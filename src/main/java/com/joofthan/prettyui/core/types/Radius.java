package com.joofthan.prettyui.core.types;

public class Radius extends AbstractValueType<Integer>{

    public Radius(int size) {
        super(size);
    }

    public static Radius of(int size){
        if(size < 0) {
            throw new InvalidValueException("Negative values are not allowed in Radius.");
        }
        return new Radius(size);
    }
}
