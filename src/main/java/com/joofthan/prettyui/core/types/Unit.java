package com.joofthan.prettyui.core.types;

public enum Unit {
    VIEW_WIDTH("vw"),
    VIEW_HEIGTH("vh"),
    FONT_SIZE("em"),
    PIXEL("px")
    ;

    private final String value;

    Unit(String value) {
        this.value = value;
    }

    public String toString(){
        return this.value;
    }
}
