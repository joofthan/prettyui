package com.joofthan.prettyui.core;

import com.joofthan.prettyui.core.types.EventType;
import com.joofthan.prettyui.core.style.UiStyle;

public interface UiElement{

    String generateHTML(RenderContext context);

    String generateCSS(RenderContext context);

    String generateJS(RenderContext context);

    int getBottomY();

    int getRightX();

    int getLeftX();

    int getTopY();

    void validateSelf();

    void handleEvent(EventType eventType);

    void setPosition(Integer x, Integer y);

    void addStyle(UiStyle style);
}
