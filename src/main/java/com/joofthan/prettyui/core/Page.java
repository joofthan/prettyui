package com.joofthan.prettyui.core;

import com.joofthan.prettyui.core.types.EventType;
import com.joofthan.prettyui.core.exception.EventException;

import java.util.ArrayList;
import java.util.List;

import static com.joofthan.prettyui.core.types.Unit.VIEW_WIDTH;
import static com.joofthan.prettyui.core.util.Util.NEW_LINE;
import static com.joofthan.prettyui.core.util.Util.blank;

public class Page implements UiPage {
    private static final String LANGUAGE = "de";
    List<UiElement> elements = new ArrayList<>();
    private final String title;
    private final RenderContext renderContext;
    private final String pageId;

    public Page(String title, RenderContext renderContext, String pageId) {
        this.title = title;
        this.renderContext = renderContext;
        this.pageId = pageId;
    }

    @Override
    public String generateHTML() {
        return generateHTML(renderContext);
    }

    private String generateHTML(RenderContext c) {
        PageData d = generatePageData();

        return "<!DOCTYPE html>\n" +
                "<html lang=\""+LANGUAGE+"\">\n" +
                "  <head>\n" +
                "    <meta charset=\"utf-8\">\n" +
                "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" +
                "    <title>"+title+"</title>\n" +
                "    <script type=\"module\" >\n" +
                "      import Lib from '"+c.resources().libJs()+"';\n" +
                "        \n" +
                "      let lib = new Lib('"+pageId+"','"+c.resources().eventEndpointURL()+"');\n" +
                "      function init() {\n" +
                         d.script +
                "      }\n" +
                "      window.init = init;\n" +
                "      lib.output('lib.mjs loaded');\n" +
                "    </script>\n" +
                "    <style id=\"style\">\n" +
                       d.css +
                "    </style>\n" +
                "  </head>\n" +
                "  <body onload='init()'>\n" +
                "    <div id=\"content\">\n" +
                       d.html +
                "    </div>\n" +
                "  </body>\n" +
                "</html>";
    }

    public PageData generatePageData() {
        RenderContext c = this.renderContext;
        String pageCSS = generatePageCSS(c.withLeftIndent(blank(6))) + NEW_LINE;
        StringBuilder css = new StringBuilder();
        StringBuilder html = new StringBuilder();
        StringBuilder script = new StringBuilder();
        for(UiElement element:elements) {
            css.append(element.generateCSS(c.withLeftIndent(blank(6)))).append(NEW_LINE);
            html.append(element.generateHTML(c.withLeftIndent(blank(4)))).append(NEW_LINE);
            script.append(element.generateJS(c.withLeftIndent(blank(8)))).append(NEW_LINE);
            c = c.nextIdContext();
        }

        return new PageData(pageCSS+css, html.toString(), script.toString());
    }

    @Override
    public String getId() {
        return pageId;
    }

    @Override
    public void handleEvent(String elementId, EventType eventType) {
        int id = -1;
        try{
            id = Integer.parseInt(elementId.replace("prettyui", ""));
            elements.get(id-1).handleEvent(eventType);
        } catch (NumberFormatException e){
            throw new EventException("Invalid elementId: \""+elementId+"\".");
        } catch (IndexOutOfBoundsException e){
            throw new EventException("elements.contains("+id+"-1) with size " + elements.size() + " does not contain element with id: "+id+".", elements, this);
        }
    }

    private String generatePageCSS(RenderContext c) {
        String style =  NEW_LINE +
                        c.line("* {") +
                        c.line("  margin: 0;") +
                        c.line("  padding: 0;") +
                        c.line("}");
        if(VIEW_WIDTH.equals(c.getUnit())){
            style +=    NEW_LINE +
                        c.line("body {")+
                        c.line("  width: 100vw;") +
                        c.line("  height: 100vh;") +
                        c.line("}");
        }

        return style;
    }

    @Override
    public void addElement(UiElement uiElement) {
        uiElement.validateSelf();
        elements.add(uiElement);
    }

    @Override
    public String toString() {
        return "PageDefault{" +
                "elements=" + elements +
                ", title='" + title + '\'' +
                '}';
    }
}
