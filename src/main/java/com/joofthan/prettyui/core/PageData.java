package com.joofthan.prettyui.core;

import java.util.Objects;

public class PageData {
    public final String css;
    public final String html;
    public final String script;

    public PageData(String css, String html, String script) {
        this.css = css;
        this.html = html;
        this.script = script;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PageData pageData = (PageData) o;
        return Objects.equals(css, pageData.css) && Objects.equals(html, pageData.html) && Objects.equals(script, pageData.script);
    }

    @Override
    public int hashCode() {
        return Objects.hash(css, html, script);
    }
}
