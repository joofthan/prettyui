package com.joofthan.prettyui.core.style;

import com.joofthan.prettyui.core.RenderContext;

public interface LinePart {
    String generateCSSLinePart(RenderContext c);

    String getId();
}
