package com.joofthan.prettyui.core.style;

import com.joofthan.prettyui.core.exception.PrettyUiException;

class PropertyCastException extends PrettyUiException {
    public PropertyCastException(String message) {
        super(message);
    }
}
