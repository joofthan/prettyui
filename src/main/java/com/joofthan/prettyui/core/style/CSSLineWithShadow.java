package com.joofthan.prettyui.core.style;

import com.joofthan.prettyui.core.RenderContext;
import com.joofthan.prettyui.core.exception.UiRenderException;
import com.joofthan.prettyui.core.types.Color;
import com.joofthan.prettyui.core.types.Radius;
import com.joofthan.prettyui.core.types.Size;

class CSSLineWithShadow extends AbstractSizeLessCSSLine implements CSSLine {
    public static final String BOX_SHADOW_ID = "box-shadow";
    private final boolean inset;
    private Size shadowOffsetX;
    private Size shadowOffsetY;
    private Radius blurRadius;
    private Radius spreadRadius;
    private Color color;

    public CSSLineWithShadow(Color color) {
        this(false, color);
    }

    public CSSLineWithShadow(boolean inset, Color color) {
        this(inset, Size.of(0), Size.of(0), Radius.of(0), Radius.of(0), color);
    }

    public CSSLineWithShadow(boolean inset, Size shadowOffsetX, Size shadowOffsetY, Radius blurRadius, Radius spreadRadius, Color color) {
        this.inset = inset;
        this.shadowOffsetX = shadowOffsetX;
        this.shadowOffsetY = shadowOffsetY;
        this.blurRadius = blurRadius;
        this.spreadRadius = spreadRadius;
        this.color = color;
        if(this.color == null){
            throw new UiRenderException("Color is null", null, this);
        }
    }

    public CSSLineWithShadow(CSSLineWithShadow o) {
        this(o.inset, o.shadowOffsetX, o.shadowOffsetY, o.blurRadius, o.spreadRadius, o.color);
    }

    @Override
    public String generateCSSLine(RenderContext c) {
        return c.line("box-shadow: " + (inset ? "inset" : "") +
                                            " " + shadowOffsetX.getValue() +c.getUnit() +
                                            " " + shadowOffsetY.getValue() +c.getUnit() +
                                            " " + blurRadius.getValue() +c.getUnit() +
                                            " " + spreadRadius.getValue() +c.getUnit() +
                                            " " + color.getHexCode() + ";");
    }

    @Override
    public String getPropertyKey() {
        return BOX_SHADOW_ID;
    }

    public CSSLineWithShadow offsetX(Size shadowOffsetX) {
        CSSLineWithShadow copy = new CSSLineWithShadow(this);
        this.shadowOffsetX = shadowOffsetX;
        return copy;
    }

    public CSSLineWithShadow offsetY(Size shadowOffsetY) {
        CSSLineWithShadow copy = new CSSLineWithShadow(this);
        this.shadowOffsetY = shadowOffsetY;
        return copy;
    }

    public CSSLineWithShadow blurRadius(Radius blurRadius) {
        CSSLineWithShadow copy = new CSSLineWithShadow(this);
        this.blurRadius = blurRadius;
        return copy;
    }

    public CSSLineWithShadow spreadRadius(Radius spreadRadius) {
        CSSLineWithShadow copy = new CSSLineWithShadow(this);
        this.spreadRadius = spreadRadius;
        return copy;
    }

    public CSSLineWithShadow color(Color color) {
        CSSLineWithShadow copy = new CSSLineWithShadow(this);
        copy.color = color;
        return copy;
    }

    @Override
    public String toString() {
        return "LineWithShadow{" +
                "inset=" + inset +
                ", shadowOffsetX=" + shadowOffsetX +
                ", shadowOffsetY=" + shadowOffsetY +
                ", blurRadius=" + blurRadius +
                ", spreadRadius=" + spreadRadius +
                ", color=" + color +
                '}';
    }
}
