package com.joofthan.prettyui.core.style;

import com.joofthan.prettyui.core.RenderContext;
import com.joofthan.prettyui.core.types.Size;

/**
 * Implemented by Style
 */
public interface UiStyle {
    String generateCSSProperties(RenderContext c);
    Size getOffsetWidth();
    Size getOffsetHeight();
}
