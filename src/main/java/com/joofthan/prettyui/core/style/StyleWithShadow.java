package com.joofthan.prettyui.core.style;

import com.joofthan.prettyui.core.exception.UiRenderException;
import com.joofthan.prettyui.core.types.Color;
import com.joofthan.prettyui.core.types.Radius;
import com.joofthan.prettyui.core.types.Size;

public class StyleWithShadow extends Style implements UiStyle {

    StyleWithShadow(Style b){
        super(b.cssLines);
    }

    StyleWithShadow(Style b, CSSLineWithShadow newProperty){
        super(b.cssLines);
    }

    public StyleWithShadow andShadowColor(Color color) {
        return new StyleWithShadow(this, getLineWithShadow().color(color));
    }

    public StyleWithShadow andShadowMovedToRight(int shadowOffsetX) {
        return new StyleWithShadow(this, getLineWithShadow().offsetX(Size.of(shadowOffsetX)));
    }

    public StyleWithShadow andShadowMovedToBottom(int shadowOffsetY) {
        return new StyleWithShadow(this, getLineWithShadow().offsetY(Size.of(shadowOffsetY)));
    }

    public StyleWithShadow andShadowBlur(int size) {
        return new StyleWithShadow(this, getLineWithShadow().blurRadius(Radius.of(size)));
    }

    public StyleWithShadow andShadowSize(int size) {
        return new StyleWithShadow(this, getLineWithShadow().spreadRadius(Radius.of(size)));
    }

    private CSSLineWithShadow getLineWithShadow(){
        CSSLine line = this.cssLines.get(CSSLineWithShadow.BOX_SHADOW_ID);
        if(line == null){
            throw new UiRenderException("this.properties\"Map<String, UiStylePropertyLine>\" does not contain "+ CSSLineWithShadow.BOX_SHADOW_ID, null, this.cssLines.keySet(), this.cssLines);
        }
        if(!(line instanceof CSSLineWithShadow)){
            throw new PropertyCastException(CSSLineWithShadow.BOX_SHADOW_ID+" is not type LineWithShadow: "+line.getClass().getName());
        }

        return (CSSLineWithShadow) line;
    }
}
