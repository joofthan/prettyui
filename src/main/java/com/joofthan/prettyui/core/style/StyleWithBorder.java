package com.joofthan.prettyui.core.style;

import com.joofthan.prettyui.core.types.Color;
import com.joofthan.prettyui.core.types.Radius;
import com.joofthan.prettyui.core.types.Size;

public class StyleWithBorder extends Style implements UiStyle {

    StyleWithBorder(Style b){
        super(b.cssLines);
    }

    public StyleWithBorder andBorderRadius(int s) {
        Radius size = Radius.of(s);
        return new StyleWithBorder(copyWithProperty(new CSSLineImpl("border-radius: "+size.getValue(), true)));
    }

    public StyleWithBorder andBorderWidth(int w) {
        Size width = Size.of(w);
        return new StyleWithBorder(copyWithProperty(new CSSLineImpl("border-width: "+ width.getValue(), true, width.multiply(2), width.multiply(2))));
    }

    public StyleWithBorder andBorderColor(Color color) {
        return new StyleWithBorder(copyWithProperty(new CSSLineImpl("border-color: "+color.getHexCode())));
    }
}
