package com.joofthan.prettyui.core.style;

import com.joofthan.prettyui.core.RenderContext;
import com.joofthan.prettyui.core.types.Size;

public interface CSSLine {
    String generateCSSLine(RenderContext c);

    Size getOffsetWidth();

    Size getOffsetHeight();

    String getPropertyKey();
}
