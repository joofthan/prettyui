package com.joofthan.prettyui.core.style;

public enum BorderStyle {
    SOLID("solid"), DOTTED("dotted"), DOUBLE("double"), DASHED("dashed");

    private final String value;

    BorderStyle(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
