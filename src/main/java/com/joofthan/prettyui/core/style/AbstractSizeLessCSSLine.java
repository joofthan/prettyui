package com.joofthan.prettyui.core.style;

import com.joofthan.prettyui.core.types.Size;

public abstract class AbstractSizeLessCSSLine implements CSSLine {
    @Override
    public Size getOffsetWidth() {
        return Size.ZERO;
    }

    @Override
    public Size getOffsetHeight() {
        return Size.ZERO;
    }
}
