package com.joofthan.prettyui.core.style;

import static com.joofthan.prettyui.core.style.BorderStyle.SOLID;
import static com.joofthan.prettyui.core.types.Color.*;

public class Styles {
    public static Style MOVE_UP = new Style().withPositionOffset(0, -3);
    public static Style MOVE_DOWN = new Style().withPositionOffset(0, 3);
    public static Style MOVE_LEFT = new Style().withPositionOffset(-3, 0);
    public static Style MOVE_RIGHT = new Style().withPositionOffset(3, 0);
    public static StyleWithShadow GLOW_BLUE = new Style().withShadowColor(BLUE).andShadowBlur(3);
    public static StyleWithShadow GLOW_GREEN = new Style().withShadowColor(GREEN).andShadowBlur(3);
    public static StyleWithBorder BORDER_BLACK = new Style().withBorderType(SOLID).andBorderColor(BLACK);
    public static StyleWithBorder BORDER_ROUND = new Style().withBorderType(SOLID).andBorderRadius(3);
}
