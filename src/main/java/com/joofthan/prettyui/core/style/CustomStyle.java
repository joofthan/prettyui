package com.joofthan.prettyui.core.style;

import com.joofthan.prettyui.PrettyUi;
import com.joofthan.prettyui.core.RenderContext;
import com.joofthan.prettyui.core.exception.UiRenderException;
import com.joofthan.prettyui.core.types.Color;
import com.joofthan.prettyui.core.types.Rotation;
import com.joofthan.prettyui.core.types.Scale;
import com.joofthan.prettyui.core.types.Size;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;


public class CustomStyle implements UiStyle {
    private final String style;

    public CustomStyle(String style) {
        this.style = style;
        PrettyUi.getInstance().getOutputText().warn("Unchecked Style Used");
    }



    @Override
    public Size getOffsetWidth() {
        return Size.ZERO;
    }

    @Override
    public Size getOffsetHeight() {
        return Size.ZERO;
    }


    @Override
    public String generateCSSProperties(RenderContext c) {
        return Arrays.stream(style.split("\n")).map(s -> c.line(s)).collect(Collectors.joining());
    }

    @Override
    public String toString() {
        return "CustomStyle{" +
                "style=" + style +
                '}';
    }
}
