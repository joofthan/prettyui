package com.joofthan.prettyui.core.style;

import com.joofthan.prettyui.core.RenderContext;
import com.joofthan.prettyui.core.types.Value;

class LinePartImpl implements LinePart {
    private final String property;
    private final Value value1;
    private final Value value2;
    private final boolean addUnit;
    private final String customUnit;

    public LinePartImpl(String property, Value value1, Value value2, boolean addUnit, String customUnit) {
        this.property = property;
        this.value1 = value1;
        this.value2 = value2;
        this.addUnit = addUnit;
        this.customUnit = customUnit;
    }

    public LinePartImpl(String property, Value value1, Value value2, boolean addUnit) {
        this(property, value1, value2, addUnit, null);
    }

    public LinePartImpl(String property, Value value, boolean addUnit) {
        this(property, value, null, addUnit, null);
    }

    public LinePartImpl(String property, Value value, String customValue) {
        this(property, value, null,false, customValue);
    }


    @Override
    public String generateCSSLinePart(RenderContext c) {
        if(value2 == null){
            return property+"("+withUnit(value1, c)+")";
        }else{
            return property+"("+withUnit(value1, c)+", "+withUnit(value2, c)+")";
        }
    }

    private String withUnit(Value v, RenderContext c) {
        if(customUnit == null) {
            return v.getValue() + (addUnit?c.getUnit():"");
        }else{
            return v.getValue() + customUnit;
        }
    }

    @Override
    public String getId() {
        return this.property;
    }

    @Override
    public String toString() {
        return "PartDefault{" +
                "property='" + property + '\'' +
                ", value1=" + value1 +
                ", value2=" + value2 +
                ", addUnit=" + addUnit +
                ", customUnit='" + customUnit + '\'' +
                '}';
    }
}
