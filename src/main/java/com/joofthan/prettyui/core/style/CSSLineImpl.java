package com.joofthan.prettyui.core.style;

import com.joofthan.prettyui.core.RenderContext;
import com.joofthan.prettyui.core.types.Size;

class CSSLineImpl implements CSSLine {
    private final boolean addUnit;
    private final String propertyValue;
    private final Size widthOffset;
    private final Size heightOffset;

    public CSSLineImpl(String property) {
        this(property, false, Size.ZERO, Size.ZERO);
    }

    public CSSLineImpl(String property, boolean addUnit) {
        this(property, addUnit, Size.ZERO, Size.ZERO);
    }

    public CSSLineImpl(String property, boolean addUnit, Size widthOffset, Size heightOffset) {
        this.propertyValue = property;
        this.addUnit = addUnit;
        this.widthOffset = widthOffset;
        this.heightOffset = heightOffset;
    }

    @Override
    public String generateCSSLine(RenderContext c){
        return c.line(this.propertyValue + (addUnit?c.getUnit():"")+";");
    }

    @Override
    public Size getOffsetWidth() {
        return widthOffset;
    }

    @Override
    public Size getOffsetHeight() {
        return heightOffset;
    }

    @Override
    public String getPropertyKey() {
        return this.propertyValue.split(":")[0];
    }

    @Override
    public String toString() {
        return "LineDefault{" +
                "addUnit=" + addUnit +
                ", propertyValue='" + propertyValue + '\'' +
                ", widthOffset=" + widthOffset +
                ", heightOffset=" + heightOffset +
                '}';
    }
}
