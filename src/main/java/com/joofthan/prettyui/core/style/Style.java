package com.joofthan.prettyui.core.style;

import com.joofthan.prettyui.core.RenderContext;
import com.joofthan.prettyui.core.exception.UiRenderException;
import com.joofthan.prettyui.core.types.Color;
import com.joofthan.prettyui.core.types.Rotation;
import com.joofthan.prettyui.core.types.Scale;
import com.joofthan.prettyui.core.types.Size;

import java.util.HashMap;
import java.util.Map;


public class Style implements UiStyle {
    Map<String, CSSLine> cssLines = new HashMap<>();

    public Style() {
    }

    Style(Map<String, CSSLine> cssLines) {
        cssLines.forEach((key, value) -> this.cssLines.put(key, value));
    }

    public Style(Style other) {
        this(other.cssLines);
    }


    public StyleWithBorder withBorderType(BorderStyle borderStyle) {
        return new StyleWithBorder(copyWithProperty(new CSSLineImpl("border-style: "+borderStyle.getValue())));
    }


    public StyleWithShadow withShadowColor(Color color) {
        return new StyleWithShadow(copyWithProperty(new CSSLineWithShadow(color)));
    }

    protected Style copyWithProperty(CSSLine cssLine) {
        Style copy = new Style(cssLines);
        copy.cssLines.put(cssLine.getPropertyKey() , cssLine);
        return copy;
    }

    @Override
    public Size getOffsetWidth() {
        return this.cssLines.values().stream()
                .map(CSSLine::getOffsetWidth)
                .reduce(Size.ZERO, Size::add);
    }

    @Override
    public Size getOffsetHeight() {
        return this.cssLines.values().stream()
                .map(CSSLine::getOffsetWidth)
                .reduce(Size.ZERO, Size::add);
    }

    public Style and(Style style) {
        Style copy = new Style(this);
        copy.cssLines.putAll(style.cssLines);
        return copy;
    }

    @Override
    public String generateCSSProperties(RenderContext c) {
        return cssLines.values().stream()
                .map(prop -> tryGenerate(c, prop))
                .reduce("",String::concat);
    }

    private String tryGenerate(RenderContext c, CSSLine prop) {
        try{
            return prop.generateCSSLine(c.addLeftIndent("  "));
        }catch (UiRenderException e){
            throw new UiRenderException("Error on call Line.generateCSSLine.", e, prop);
        }

    }

    public Style withPositionOffset(int x, int y) {
        return copyWithSubProperty(CSSLineWithTransform.TRANSFORM_KEY, new LinePartImpl("translate", Size.of(x), Size.of(y), true));
    }

    public Style withScale(double value) {
        return copyWithSubProperty(CSSLineWithTransform.TRANSFORM_KEY, new LinePartImpl("scale", Scale.of(value), false));
    }

    public Style withRotation(int value) {
        return copyWithSubProperty(CSSLineWithTransform.TRANSFORM_KEY, new LinePartImpl("rotate", Rotation.of(value), "deg"));
    }

    private Style copyWithSubProperty(String id, LinePart linePart) {
        Style styleCopy = new Style(this.cssLines);
        CSSLineWithTransform line = (CSSLineWithTransform) this.cssLines.getOrDefault(id, new CSSLineWithTransform());
        styleCopy.cssLines.put(id, line.withSubProperty(linePart));
        return styleCopy;
    }

    @Override
    public String toString() {
        return "StyleBuilder{" +
                "properties=" + cssLines +
                '}';
    }
}
