package com.joofthan.prettyui.core.style;

import com.joofthan.prettyui.core.RenderContext;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

class CSSLineWithTransform extends AbstractSizeLessCSSLine implements CSSLine {
    public static final String TRANSFORM_KEY = "transform";
    private final Map<String, LinePart> transformProperties = new HashMap<>();

    public CSSLineWithTransform(Map<String, LinePart> properties) {
        this.transformProperties.putAll(properties);
    }

    public CSSLineWithTransform() {
    }

    @Override
    public String generateCSSLine(RenderContext c) {
        return c.line("transform: "+transformProperties.values().stream()
                .map(linePart -> linePart.generateCSSLinePart(c))
                .collect(Collectors.joining(" ")) +
                ";");
    }

    @Override
    public String getPropertyKey() {
        return TRANSFORM_KEY;
    }

    public CSSLineWithTransform withSubProperty(LinePart part) {
        CSSLineWithTransform line = new CSSLineWithTransform(this.transformProperties);
        line.transformProperties.put(part.getId(), part);
        return line;
    }

    @Override
    public String toString() {
        return "LineWithTransform{" +
                "transformProperties=" + transformProperties +
                '}';
    }
}
