package com.joofthan.prettyui.core;

import java.util.ArrayList;
import java.util.List;

public class PageBuilder {
    private String title;
    List<UiElement> elements = new ArrayList<>();
    private RenderContext renderContext;
    private final String pageId;

    public PageBuilder(RenderContext renderContext, String pageId) {
        this.renderContext = renderContext;
        this.pageId = pageId;
    }

    public PageBuilder withTitle(String title) {
        this.title = title;
        return this;
    }

    public PageBuilder withElement(UiElement uiElement) {
        uiElement.validateSelf();
        this.elements.add(uiElement);
        return this;
    }


    public UiPage build() {

        UiPage page = new Page(title, renderContext, pageId);
        elements.forEach(page::addElement);

        return page;
    }

    @Override
    public String toString() {
        return "PageBuilder{" +
                "title='" + title + '\'' +
                ", elements=" + elements +
                '}';
    }
}
