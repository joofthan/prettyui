package com.joofthan.prettyui.core.io;

public class Gateway {
    private final String gatewayURL;
    public OutputText outputText;

    public Gateway(String gatewayURL, OutputText outputText) {
        if(gatewayURL == null || gatewayURL.isEmpty()){
            throw new IllegalArgumentException("gatewayURL is null");
        }
        if(!gatewayURL.startsWith("/")) throw new IllegalArgumentException("gatewayURL should start with \"/\"");
        if(gatewayURL.endsWith("/")) throw new IllegalArgumentException("gatewayURL should not end with \"/\"");

        this.gatewayURL = gatewayURL;
        this.outputText = outputText;
    }

    public String url(){
        return gatewayURL+"/";
    }
}
