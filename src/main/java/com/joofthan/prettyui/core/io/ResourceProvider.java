package com.joofthan.prettyui.core.io;

public interface ResourceProvider {
    String getContent(String path);

    byte[] getBytes(String path);
}
