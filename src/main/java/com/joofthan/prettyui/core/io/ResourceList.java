package com.joofthan.prettyui.core.io;

import java.security.cert.CertPathChecker;
import java.util.List;

public interface ResourceList  {
    List<String> logContent();

    String libJs();

    String eventEndpointURL();

    Images img();
}
