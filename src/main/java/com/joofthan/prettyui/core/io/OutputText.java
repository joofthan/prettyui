package com.joofthan.prettyui.core.io;

import java.util.List;

public interface OutputText {
    void info(String text);
    void warn(String text);
    void error(String text);

    void fine(String text);

    void text(String text);

    List<String> log();

    void finest(String text);
}
