package com.joofthan.prettyui.core.io;

public interface Images {
    String check();
    String checkHover();
    String checkEmpty();
    String checkEmptyHover();
    String folderURL();
}
