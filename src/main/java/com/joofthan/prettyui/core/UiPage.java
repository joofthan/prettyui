package com.joofthan.prettyui.core;

import com.joofthan.prettyui.core.types.EventType;

public interface UiPage {
    void addElement(UiElement uiElement);
    String generateHTML();

    String getId();

    void handleEvent(String elementId, EventType eventType);

    PageData generatePageData();
}
