package com.joofthan.prettyui.core.util;

import java.util.Arrays;
import java.util.stream.Collectors;

public class Util {
    public static final String NEW_LINE = "\n";

    public static String blank(int c) {
        return " ".repeat(c);
    }

    public static String htmlFormattingObjects(Object[] ob) {
        if(ob == null) return "";
        return Arrays.stream(ob)
                .map(o -> "<b>"+o.getClass()+"</b><br>"+htmlFormatting(""+o))
                .collect(Collectors.joining("<br><br>"));
    }

    public static String htmlFormatting(String text) {
        if(text == null) return "";
        return text.replace("<", "&lt;")
                    .replace(">", "&gt;")
                    .replace("{", "{<details style=\"display:inline;\"><summary style=\"color:blue;cursor: pointer;\">show</summary><span style=\"color: gray;\">")
                    .replace("}", "</span></details>}")
                    .replace("[", "[<br>")
                    .replace("]", "<br>]")
                    .replace(", ",",<br>")
                +"<style>details[open] {\n" +
                "border:solid 1px blue;border-radius: 0.5em;display:block !important;margin-left:1em;"+
                "}</style>";
    }
}
