package com.joofthan.prettyui.core.exception;

public class EventException extends PrettyUiException {
    public EventException(String message, Object...objects) {
        super(message, null, objects);
    }
}
