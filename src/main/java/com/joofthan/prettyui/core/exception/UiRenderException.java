package com.joofthan.prettyui.core.exception;

public class UiRenderException extends PrettyUiException {
    public UiRenderException(String message) {
        super(message);
    }

    public UiRenderException(String message, Exception e) {
        super(message, e);
    }

    public UiRenderException(String message, Exception e, Object... data) {
        super(message, e, data);
    }

    public UiRenderException(String message, Object...  data) {
        super(message, null, data);
    }
}
