package com.joofthan.prettyui.core.exception;

import com.joofthan.prettyui.core.util.Util;

public class PrettyUiException extends RuntimeException {
    private Object[] additionalObjects;

    public PrettyUiException() {
        this(null, null);
    }

    public PrettyUiException(String message) {
        this(message, null);
    }

    public PrettyUiException(String message, Exception e) {
        super(message, e);
    }

    public PrettyUiException(String message, Throwable e, Object... additionalObjects) {
        super(message, e);
        this.additionalObjects = additionalObjects;
    }

    public String asHTML() {
        return "<!DOCTYPE html>\n" +
                "<html lang=\"de\">\n" +
                "  <head>\n" +
                "    <meta charset=\"utf-8\">\n" +
                "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" +
                "    <title>Internal Server Error</title>" +
                "  </head>\n" +
                "  <body>\n" +
                "<h2>Page not found</h2>" +
                "<div style=\"font-size:1.1em;\">"+
                    htmlMessage(this) + "<br>"+
                    htmlMessage(getCause()) +
                "</div>"+
                    htmlStacktrace("Stacktrace: "+this.toString(), this)+
                    htmlStacktrace("Caused by: "+getCause(), getCause())+
                "<h4>Data</h4>" +
                "<div style=\"vertical-align: top;display:inline;\">" + additionalInfos() + "</div>" +
                "  </body>\n" +
                "</html>";
    }

    private String htmlMessage(Throwable e) {
        if(e == null) return "";
        return Util.htmlFormatting(e.getMessage());
    }

    protected String additionalInfos(){
        return Util.htmlFormattingObjects(additionalObjects);
    }

    private String htmlStacktrace(String heading, Throwable e) {
        if(e == null) return "";
        String html = "<h4>"+heading+"</h4><p>";

        StackTraceElement[] stackTrace = e.getStackTrace();
        for (int i = 0, stackTraceLength = stackTrace.length; i < stackTraceLength; i++) {
            StackTraceElement s = stackTrace[i];
            String text = s.toString();
            if (!text.startsWith("java.base") && !text.startsWith("jdk.httpserver")) {
                if (!text.startsWith("com.joofthan.prettyui.")) {
                    html += formatHTMLStacktrace(text,
                            "color: black;font-weight: bold;",
                            "color: #D00;font-weight:bold;")+"<br>";
                } else {
                    html += formatHTMLStacktrace(text,
                            "color: black;",
                            "color: #D00;")+"<br>";
                }

            } else {
                //html += "<span style=\"color: lightgray;\">"+s+"</span><br>";
            }
        }
        return html + "</p>";
    }

    private String formatHTMLStacktrace(String text, String style1, String style2) {
        return "<span style=\""+style1 +"\">"+
                    text.substring(0, text.indexOf("(")) +
                "</span>" +
                "<span style=\"" +style2 +  "\">" +
                    text.substring(text.indexOf("(")) +
                "</span>";
    }



    public PrettyUiException withCause(Exception e){
        return new PrettyUiException(this.getMessage(), e, this.additionalObjects);
    }
    public PrettyUiException withMessage(String text){
        return new PrettyUiException(text, this.getCause(), this.additionalObjects);
    }
    public PrettyUiException withData(Object... data){
        return new PrettyUiException(this.getMessage(), this.getCause(), data);
    }
}
