package com.joofthan.prettyui.core.exception;

public class UnsupportedMethodCall extends PrettyUiException {
    public UnsupportedMethodCall(String message) {
        super(message);
    }

    public <T> UnsupportedMethodCall(String message, Object object) {
        super(message, null ,  object);
    }
}
