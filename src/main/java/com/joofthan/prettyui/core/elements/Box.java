package com.joofthan.prettyui.core.elements;

import com.joofthan.prettyui.core.RenderContext;
import com.joofthan.prettyui.core.UiElement;
import com.joofthan.prettyui.core.types.Color;
import com.joofthan.prettyui.core.types.EventType;
import com.joofthan.prettyui.core.types.Size;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.joofthan.prettyui.core.util.Util.blank;

public class Box extends AbstractUiElementBuilder<Box> implements UiElement {

    List<UiElement> children = new ArrayList<>();
    private Color color;

    public Box() {
        super(Size.ZERO, Size.ZERO, Size.NONE, Size.NONE);
    }

    @Override
    public String generateHTML(RenderContext c) {
        return c.line("<div style=\"background-color: "+color.getHexCode()+";\" id=\""+c.elementId()+"\">") +
                (children.stream().map(e-> e.generateHTML(c.addLeftIndent(blank(2)))).collect(Collectors.joining())) +
                c.line("</div>");
    }

    @Override
    public String generateCSS(RenderContext c) {
        return c.line("#"+c.elementId()+" {") +
                generatePositionProperties(c) +
                generateCSSProperties(c) +
                c.line("}\n")+
                c.line("#"+c.elementId()+":hover {")+
                generateCSSHoverProperties(c) +
                c.line("}") +
                c.line("#"+c.elementId()+":selected {")+
                generateCSSSelectedProperties(c) +
                c.line("}");
    }

    @Override
    public void handleEvent(EventType eventType) {
        if(this.eventCallbacks.containsKey(eventType)) {
            this.eventCallbacks.get(eventType).forEach(e -> e.accept(this));
        }
    }

    public Box withColor(Color color) {
        this.color = color;
        return this;
    }

}
