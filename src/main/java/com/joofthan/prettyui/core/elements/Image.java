package com.joofthan.prettyui.core.elements;

import com.joofthan.prettyui.core.RenderContext;
import com.joofthan.prettyui.core.types.EventType;
import com.joofthan.prettyui.core.types.Size;

public class Image extends AbstractUiElementBuilder<Image> {
    private String path;

    public Image(String path, int width, int height) {
        super(Size.ZERO, Size.ZERO, Size.of(width), Size.of(height));
        this.path = path;
    }

    public Image(String path) {
        this(path, -1,-1);
    }

    Image(Image other){
        super(other);
        this.path = other.path;
    }

    @Override
    public String generateHTML(RenderContext c) {
        return c.indent()+"<img id=\""+c.elementId()+"\" src=\""+path+"\" />";
    }

    @Override
    public String generateCSS(RenderContext c) {
        return c.line("#"+c.elementId()+" {") +
                generatePositionProperties(c) +
                generateCSSProperties(c) +
                c.line("}\n")+
                c.line("#"+c.elementId()+":hover {")+
                generateCSSHoverProperties(c) +
                c.line("}") +
                c.line("#"+c.elementId()+":selected {")+
                generateCSSSelectedProperties(c) +
                c.line("}");
    }


    @Override
    public void handleEvent(EventType eventType) {
        if(this.eventCallbacks.containsKey(eventType)) {
            this.eventCallbacks.get(eventType).forEach(e -> e.accept(this));
        }
    }

    public void setImage(String path){
        this.path = path;
    }









    @Override
    public String toString() {
        return "Image{" +
                "path='" + path + '\'' +
                "} " + super.toString();
    }
}
