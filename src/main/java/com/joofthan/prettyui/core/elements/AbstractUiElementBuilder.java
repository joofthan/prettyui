package com.joofthan.prettyui.core.elements;

import com.joofthan.prettyui.core.style.UiStyle;
import com.joofthan.prettyui.core.types.Size;

import java.util.function.Consumer;

import static com.joofthan.prettyui.core.types.EventType.*;

public abstract class AbstractUiElementBuilder<TElement> extends AbstractUiElement<TElement> {
    public AbstractUiElementBuilder(Size positionX, Size positionY, Size width, Size height) {
        super(positionX, positionY, width, height);
    }

    public AbstractUiElementBuilder(AbstractUiElementBuilder<TElement> other) {
        super(other);
    }

    //Required
    public TElement withPosition(int x, int y){
        this.positionX = Size.of(x);
        this.positionY = Size.of(y);
        return (TElement) this;
    }

    //Required
    public TElement withSize(int width, int height) {
        this.height = Size.of(height);
        this.width = Size.of(width);
        return (TElement) this;
    }

    public TElement withHeight(int height) {
        this.height = Size.of(height);
        return (TElement) this;
    }

    public TElement withWidth(int width) {
        this.width = Size.of(width);
        return (TElement) this;
    }

    public TElement withAnimationDuration(int milliseconds) {
        this.animationDuration = milliseconds;
        return (TElement) this;
    }

    public TElement onClickListener(Consumer<TElement> callback) {
        this.addEventListener(ON_CLICK, callback);
        return (TElement) this;
    }

    public TElement onSelectListener(Consumer<TElement> callback) {
        this.addEventListener(ON_SELECT, callback);
        return (TElement) this;
    }

    public TElement onUnSelectListener(Consumer<TElement> callback) {
        this.addEventListener(ON_UNSELECT, callback);
        return (TElement) this;
    }

    public TElement onHoverListener(Consumer<TElement> callback) {
        this.addEventListener(ON_HOVER, callback);
        return (TElement) this;
    }

    public TElement onUnHoverListener(Consumer<TElement> callback) {
        this.addEventListener(ON_UNHOVER, callback);
        return (TElement) this;
    }

    public TElement withStyle(UiStyle style){
        this.addStyle(style);
        return (TElement) this;
    }

    public TElement withOnHoverStyle(UiStyle style){
        this.addHoverStyle(style);
        return (TElement) this;
    }

    public TElement withOnSelectStyle(UiStyle style){
        this.addSelectedStyle(style);
        return (TElement) this;
    }
}
