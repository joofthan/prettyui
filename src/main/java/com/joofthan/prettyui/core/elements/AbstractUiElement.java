package com.joofthan.prettyui.core.elements;

import com.joofthan.prettyui.core.RenderContext;
import com.joofthan.prettyui.core.UiElement;
import com.joofthan.prettyui.core.exception.UiRenderException;
import com.joofthan.prettyui.core.style.UiStyle;
import com.joofthan.prettyui.core.types.EventType;
import com.joofthan.prettyui.core.types.Size;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

abstract class AbstractUiElement<T> implements UiElement {
    protected Size positionX;
    protected Size positionY;
    protected Size width;
    protected Size height;
    protected int animationDuration = 250;
    protected final List<UiStyle> style = new ArrayList<>();
    protected final List<UiStyle> hoverStyle = new ArrayList<>();
    protected final List<UiStyle> selectedStyle = new ArrayList<>();
    protected final Map<EventType, List<Consumer<T>>> eventCallbacks = new HashMap<>();

    protected boolean elementChanged = false;

    AbstractUiElement(Size positionX, Size positionY, Size width, Size height) {
        this.positionX = positionX;
        this.positionY = positionY;
        this.width = width;
        this.height = height;
    }

    public AbstractUiElement(AbstractUiElement<T> other) {
        this.positionX = other.positionX;
        this.positionY = other.positionY;
        this.width = other.width;
        this.height = other.height;
        this.animationDuration = other.animationDuration;
        this.style.addAll(other.style);
        this.hoverStyle.addAll(other.hoverStyle);
        this.selectedStyle.addAll(other.selectedStyle);
        this.eventCallbacks.putAll(other.eventCallbacks);
    }

    public void setPosition(Integer x, Integer y) {
        this.positionX = Size.of(x);
        this.positionY = Size.of(y);
        elementChanged = true;
    }


    @Override
    public String generateJS(RenderContext c) {
        return c.line("{") +
                c.line("  let elem = document.querySelector('#"+c.elementId()+"');") +
                c.line("  if(elem === null){")+
                c.line("    lib.error('Element with id "+c.elementId()+" not found. Was id written in generateHTML?');") +
                c.line("  }else{") +
                c.line("    elem.addEventListener('click', (e) => lib.prettyUiClicked('"+c.elementId()+"', e));") +
                c.line("    elem.addEventListener('mouseover', (e) => lib.prettyUiHovered('"+c.elementId()+"', e));") +
                c.line("    elem.addEventListener('mouseout', (e) => lib.prettyUiUnHovered('"+c.elementId()+"', e));") +
                c.line("  }")+
                c.line("}");
    }

    protected String generatePositionProperties(RenderContext c) {
        return   c.line("  position: absolute;")
                +c.line("  left: "+this.positionX.getValue()+c.getUnit()+";")
                +c.line("  top: "+this.positionY.getValue()+c.getUnit()+";")
                +c.line("  width: "+ getWidth()+c.getUnit()+";")
                +c.line("  height: "+getHeight()+c.getUnit()+";")
                +c.line("  transition: "+this.animationDuration+"ms;");
    }

    public int getWidth() {
        return this.width.subtract(elementOffsetWidth()).getIntegerValue();
    }

    private Size elementOffsetWidth() {
        return this.style.stream()
                .map(UiStyle::getOffsetWidth)
                .reduce((e, r) -> e.add(r))
                .orElse(Size.ZERO);
    }

    public int getHeight() {
        return this.height.subtract(elementOffsetHeight()).getIntegerValue();
    }

    private Size elementOffsetHeight() {
        return this.style.stream()
                .map(UiStyle::getOffsetHeight)
                .reduce((e, r) -> e.add(r))
                .orElse(Size.ZERO);
    }

    protected String generateCSSHoverProperties(RenderContext c) {
        return this.hoverStyle.stream()
                .map(prop -> prop.generateCSSProperties(c))
                .reduce("", String::concat);
    }

    protected String generateCSSSelectedProperties(RenderContext c) {
        return this.selectedStyle.stream()
                .map(prop -> prop.generateCSSProperties(c))
                .reduce("", String::concat);
    }

    protected String generateCSSProperties(RenderContext c) {
        return this.style.stream()
                .map(prop -> prop.generateCSSProperties(c))
                .reduce("", String::concat);
    }


    public int getBottomY() {
        return this.positionY.add(this.height).getIntegerValue();
    }


    public int getRightX() {
        return this.positionX.add(this.width).getIntegerValue();
    }

    public int getLeftX() {
        return this.positionX.getIntegerValue();
    }

    public int getTopY() {
        return this.positionY.getIntegerValue();
    }

    public void setAnimationDuration(int milliseconds) {
        this.animationDuration = milliseconds;
        elementChanged = true;
    }

    public void addStyle(UiStyle uiStyle) {
        this.style.add(uiStyle);
        elementChanged = true;
    }

    public void addHoverStyle(UiStyle style) {
        this.hoverStyle.add(style);
        elementChanged = true;
    }

    public void addSelectedStyle(UiStyle style) {
        this.selectedStyle.add(style);
        elementChanged = true;
    }

    public void addEventListener(EventType type, Consumer<T> callback) {
        if (!this.eventCallbacks.containsKey(type)) {
            this.eventCallbacks.put(type, new ArrayList<>());
        }
        this.eventCallbacks.get(type).add(callback);
    }

    @Override
    public void validateSelf() {
        if(width.getIntegerValue()<0 || height.getIntegerValue() < 0){
            throw new UiRenderException("UiElement does not have a Size", this);
        }
    }

    @Override
    public String toString() {
        return "AbstractUiElement{" +
                "positionX=" + positionX +
                ", positionY=" + positionY +
                ", width=" + width +
                ", height=" + height +
                ", animationDuration=" + animationDuration +
                ", style=" + style +
                ", hoverStyle=" + hoverStyle +
                ", selectedStyle=" + selectedStyle +
                ", eventCallbacks=" + eventCallbacks +
                '}';
    }

}
