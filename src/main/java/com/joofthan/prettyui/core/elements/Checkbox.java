package com.joofthan.prettyui.core.elements;

import com.joofthan.prettyui.core.RenderContext;
import com.joofthan.prettyui.core.types.EventType;
import com.joofthan.prettyui.core.types.Size;

public class Checkbox extends AbstractUiElementBuilder<Checkbox> {

    private boolean selected;

    public Checkbox() {
        super(Size.ZERO, Size.ZERO, Size.NONE, Size.NONE);
        this.animationDuration = 0;
    }

    @Override
    public String generateHTML(RenderContext c) {
        return c.line("<input id=\""+c.elementId()+"input\" type=\"checkbox\" "+(selected?"checked=\"checked\"":"")+">") +
                c.line("<label id=\""+c.elementId()+"\" for=\""+c.elementId()+"input\" class=\"container\"></label>");
    }

    @Override
    public String generateJS(RenderContext c){
        return super.generateJS(c)+
                c.line("{" +
                                "document.querySelector('#"+c.elementId()+"').addEventListener('click', (e)=>{" +
                                    "if(document.querySelector('#"+c.elementId()+"input').checked){"+
                                        "lib.prettyUiUnSelected('"+c.elementId()+"', e);" +
                                    "}else{" +
                                        "lib.prettyUiSelected('"+c.elementId()+"', e);" +
                                    "}" +
                                "});" +
                            "}");
    }

    @Override
    public String generateCSS(RenderContext c) {
        return c.line("#"+c.elementId()+"input {") +
                c.line("  position: absolute;")+
                c.line("  left: "+this.positionX.getValue()+c.getUnit()+";")+
                c.line("  top: "+this.positionY.getValue()+c.getUnit()+";")+
                c.line("  opacity: 0;") +
                c.line("  cursor: pointer;") +
                c.line("  height: 0;") +
                c.line("  width: 0;") +
                c.line("}") +

                c.line("#"+c.elementId()+"input + label {") +
                generatePositionProperties(c) +
                generateCSSProperties(c) +
                c.line("  display: block;") +
                c.line("  cursor: pointer;") +
                c.line("  user-select: none;") +
                c.line("  background-size: 100% 100%;") +
                c.line("  background-image: url(\""+c.resources().img().checkEmpty().substring(1)+"\");") +
                c.line("}\n")+

                c.line("#"+c.elementId()+"input:checked + label {") +
                generateCSSSelectedProperties(c) +
                c.line("  background-image: url(\""+c.resources().img().check().substring(1)+"\");") +
                c.line("}")+

                c.line("#"+c.elementId()+"input + label:hover {")+
                c.line("  background-image: url(\""+c.resources().img().checkEmptyHover().substring(1)+"\");") +
                generateCSSHoverProperties(c) +
                c.line("}")+

                c.line("#"+c.elementId()+"input:checked + label:hover {")+
                c.line("  background-image: url(\""+c.resources().img().checkHover().substring(1)+"\");") +
                generateCSSHoverProperties(c) +
                c.line("}");
    }


    @Override
    public void handleEvent(EventType eventType) {
        if(eventType == EventType.ON_SELECT){
            this.selected = true;
        }
        if(eventType == EventType.ON_UNSELECT){
            this.selected = false;
        }

        if(this.eventCallbacks.containsKey(eventType)) {
            this.eventCallbacks.get(eventType).forEach(e -> e.accept(this));
        }
    }

    public boolean getSelected(){
        return !selected;
    }
}
