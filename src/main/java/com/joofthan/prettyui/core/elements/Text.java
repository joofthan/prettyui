package com.joofthan.prettyui.core.elements;

import com.joofthan.prettyui.core.RenderContext;
import com.joofthan.prettyui.core.UiElement;
import com.joofthan.prettyui.core.types.Color;
import com.joofthan.prettyui.core.types.EventType;
import com.joofthan.prettyui.core.types.Size;

public class Text extends AbstractUiElementBuilder<Text> implements UiElement {

    private String text;
    private Color color = Color.BLACK;
    private Size fontSize = Size.of(3);

    public Text(String text) {
        super(Size.NONE, Size.NONE, Size.NONE, Size.NONE);
        this.text = text;
    }

    @Override
    public String generateHTML(RenderContext c) {
        return c.line("<span id=\""+c.elementId()+"\">"+text+"</span>");
    }

    @Override
    public String generateCSS(RenderContext c) {
        return c.line("#"+c.elementId()+" {") +
                generatePositionProperties(c) +
                generateCSSProperties(c) +
                c.line("color: "+this.color.getHexCode()+";")+
                c.line("font-size: "+this.fontSize.getValue()+c.getUnit()+";")+
                c.line("}\n")+
                c.line("#"+c.elementId()+":hover {")+
                generateCSSHoverProperties(c) +
                c.line("}") +
                c.line("#"+c.elementId()+":selected {")+
                generateCSSSelectedProperties(c) +
                c.line("}");
    }

    @Override
    public void handleEvent(EventType eventType) {
        if(this.eventCallbacks.containsKey(eventType)) {
            this.eventCallbacks.get(eventType).forEach(e -> e.accept(this));
        }
    }

    public Text withColor(Color color) {
        this.color = color;
        return this;
    }

    public Text withFontSize(int size){
        this.fontSize = Size.of(size);
        return this;
    }

    public void setColor(Color color){
        this.color = color;
    }

    public void setFontSize(int size){
        this.fontSize = Size.of(size);
    }

    public void setValue(String text){
        this.text = text;
    }
}
