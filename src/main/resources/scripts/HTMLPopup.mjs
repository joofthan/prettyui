export default class HTMLPopup{
    _title;
    _description;
    _content;
    body;
    blocker;
    popupDiv;
    closeButton;
    constructor(title, description, content) {
        this._title = title;
        this._description = description;
        this._content = content;
        this._init();
        this.show();
    }

    _init(){
        this.body = document.querySelector('body');

        this.blocker = document.createElement('div' );
        this.blocker.style.position = 'absolute';
        this.blocker.style.width = '100vw';
        this.blocker.style.height = '100vh';
        this.blocker.style.backgroundColor = 'rgba(0,0,0, 0.1)';

        this.popupDiv = document.createElement('div' );
        this.popupDiv.innerHTML = '<h1>'+this._title+'</h1><br>'+this._description+'<br><br>' + this._content+'<br><br>';
        let css = this.popupDiv.style;
        css.position = 'absolute';
        css.left = '10vw';
        css.top = '5vh';
        css.width = 'calc(80vw - 1em)';
        css.height = 'calc(80vh - 1em)';
        css.padding = '1em';
        css.backgroundColor = 'white';
        css.borderRadius = '1em';
        css.boxShadow = '0em 0em 1em black';
        css.overflow = 'auto';

        this.closeButton = document.createElement('div' );
        this.closeButton.innerText = 'Close';
        this.closeButton.style.position = 'absolute';
        this.closeButton.style.right = 'calc(10vw + 1em)';
        this.closeButton.style.bottom = 'calc(15vh)';
        this.closeButton.style.width = '10em';
        this.closeButton.style.height = '2em';
        this.closeButton.style.fontSize = '1em';
        this.closeButton.style.backgroundColor = 'blue';
        this.closeButton.style.color = 'white';
        this.closeButton.style.textAlign = 'center';
        this.closeButton.style.lineHeight = '2em';
        this.closeButton.style.borderRadius = '2em';
        this.closeButton.style.cursor = 'pointer';

        this.closeButton.onclick = e=> {this.close()};
        this.blocker.onclick = e=> {this.close()};
    }



    show(){
        this.body.appendChild(this.blocker);
        this.body.appendChild(this.popupDiv);
        this.body.appendChild(this.closeButton);
    }

    close(){
        this.body.removeChild(this.blocker);
        this.body.removeChild(this.popupDiv);
        this.body.removeChild(this.closeButton);
    }
}