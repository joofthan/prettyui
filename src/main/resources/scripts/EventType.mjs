/**
 * Java equivalent in com.joofthan.prettyui.core.elements.EventType
 */

const EventType = {ON_CLICK : 'clicked', ON_HOVER : 'hovered', ON_SELECT:'selected', ON_UNHOVER:'unhovered', ON_UNSELECT:'unselected'}

export default EventType;