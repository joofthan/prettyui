import EventType from "./EventType.mjs";
import HTMLPopup from "./HTMLPopup.mjs";

export default class Lib {
    pageId;
    endpointURL;

    logSize = 0;

    constructor(pageId,endpointURL) {
        this.pageId = pageId;
        this.endpointURL = endpointURL;
    }

    output(text) {
        function format(z) {
            return z < 10 ? '0' + z : z;
        }
        let d = new Date();
        let message = format(d.getHours()) +':' + format(d.getMinutes())+':'+format(d.getSeconds())+ ' SCRIPT: '+ text;
        console.log(message);
        //alert(text);
    }

    /**
     *
     * @param {string} id
     * @param {MouseEvent} event
     */
    prettyUiClicked(id, event){
        let eventUrl = this.endpointURL+this.pageId+"/"+id+"/"+EventType.ON_CLICK;
        this._updatePageWithEvent(eventUrl);
    }

    /**
     *
     * @param {string} id
     * @param {MouseEvent} event
     */
    prettyUiHovered(id, event){
        let eventUrl = this.endpointURL+this.pageId+"/"+id+"/"+EventType.ON_HOVER;
        this._updatePageWithEvent(eventUrl);
    }

    /**
     *
     * @param {string} id
     * @param {MouseEvent} event
     */
    prettyUiUnHovered(id, event){
        let eventUrl = this.endpointURL+this.pageId+"/"+id+"/"+EventType.ON_UNHOVER;
        this._updatePageWithEvent(eventUrl);
    }

    /**
     *
     * @param {string} id
     * @param {MouseEvent} event
     */
    prettyUiSelected(id, event){
        let eventUrl = this.endpointURL+this.pageId+"/"+id+"/"+EventType.ON_SELECT;
        this._updatePageWithEvent(eventUrl);
    }

    /**
     *
     * @param {string} id
     * @param {MouseEvent} event
     */
    prettyUiUnSelected(id, event){
        let eventUrl = this.endpointURL+this.pageId+"/"+id+"/"+EventType.ON_UNSELECT;
        this._updatePageWithEvent(eventUrl);
    }


    _updatePageWithEvent(eventUrl) {
        fetch(eventUrl)
            .then((r) => {
                if (r.status === 404) {
                    return r.text();
                }else {
                    return r.json();
                }
            })
            .then(result => {
                if(result.success){
                    if(result.pageChanged){
                        document.querySelector("#style").innerHTML = result.css;
                        document.querySelector("#content").innerHTML = result.html;
                        window.init();
                    }

                    for (let i = this.logSize; i < result.log.length; i++) {
                        console.log(result.log[i]);
                    }
                    this.logSize = result.log.length;
                }else{
                    let errorHtml = result.split('<body>')[1].split('</body>')[0];
                    let title = 'AJAX Error: '+eventUrl;
                    let message = 'Expected JSON but was 404 or other.';
                    this.output(title + ": "+message);
                    new HTMLPopup(title, message , errorHtml);
                }

            })
            .catch(e => {
                this.output(e);
                new HTMLPopup('AJAX Error: '+eventUrl, '', e);
            });
    }

    error(msg){
        new HTMLPopup("JavaScript Error", msg, "")
    }
}